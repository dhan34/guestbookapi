﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Operations.GuestBook.Data
{
    public class CheckinDto
    {
        public String PhoneNumber { set; get; }
        public String FirstName { set; get; }
        public String LastName { set; get; }
        public String Email { set; get; }
        public String Purpose { set; get; }

    }

}
