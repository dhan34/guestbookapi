﻿namespace Core.Operations.GuestBook.Data
{
    public class Response<T>
    {
        public T Data { set; get; }

        public string Message { set; get; }

        public bool Success { set; get; } = true;
    }
}
