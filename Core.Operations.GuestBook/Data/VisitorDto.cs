﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Operations.GuestBook.Models;

namespace Core.Operations.GuestBook.Data
{
    public class VisitorDto
    {
        public String PhoneNumber { set; get; }
        public String FirstName { set; get; }
        public String LastName { set; get; }
        public String Email { set; get; }
        public List<Visit> Visits { get; set; }
    }
}
