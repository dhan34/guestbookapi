using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Infrastructure;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Core.Operations.GuestBook.EmailServer
{
    public class EmailActions
    {
        public static bool SendEmail(string emp_mail, string date_from, string date_to)
        {
            var sendGridClient = new SendGridClient(System.Configuration.ConfigurationManager.AppSettings["GuestBookEmailAPI"]);
            var msg = new SendGridMessage();
            msg.SetTemplateId(System.Configuration.ConfigurationManager.AppSettings["AttachedEmail"]);
            msg.SetFrom(new EmailAddress(Appsettings.mailFrom));
            msg.AddTo(emp_mail);
            var dynamicTemplateData = new MailStruct
            {
                emp_mail = emp_mail,
                date_from = date_from,
                date_to = date_to

            };
            msg.SetTemplateData(dynamicTemplateData);

            if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Content"))){
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Content"));
            }
            var mypath = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/"), "Report.xlsx");
            var bytes = File.ReadAllBytes(mypath);
            var file = Convert.ToBase64String(bytes);
            msg.AddAttachment("Report.xlsx", file);
            var response = sendGridClient.SendEmailAsync(msg).Result;
            return response.StatusCode == System.Net.HttpStatusCode.Accepted ? true : false;
        }
    }
}
