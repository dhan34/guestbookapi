﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Core.Operations.GuestBook.EmailServer
{
    class MailStruct
    {
        [JsonProperty("date_from")]
        public string date_from { get; set; }

        [JsonProperty("date_to")]
        public string date_to { get; set; }

        [JsonProperty("emp_email")]
        public string emp_mail { get; set; }

        [JsonProperty("emp_name")]
        public string emp_name { get; set; }

    }
}
