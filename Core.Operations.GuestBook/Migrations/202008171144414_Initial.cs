﻿namespace Core.Operations.GuestBook.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        Email = c.String(nullable: false, maxLength: 128),
                        PasswordSalt = c.Binary(),
                        PasswordHash = c.Binary(),
                    })
                .PrimaryKey(t => t.Email);
            
            CreateTable(
                "dbo.PurposeLists",
                c => new
                    {
                        Purpose = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Purpose);
            
            CreateTable(
                "dbo.Visitors",
                c => new
                    {
                        PhoneNumber = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.PhoneNumber);
            
            CreateTable(
                "dbo.Visits",
                c => new
                    {
                        VisitId = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Purpose = c.String(nullable: false),
                        TimeIn = c.DateTime(nullable: false),
                        TimeOut = c.DateTime(),
                        Visitor_PhoneNumber = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.VisitId)
                .ForeignKey("dbo.Visitors", t => t.Visitor_PhoneNumber)
                .Index(t => t.Visitor_PhoneNumber);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Visits", "Visitor_PhoneNumber", "dbo.Visitors");
            DropIndex("dbo.Visits", new[] { "Visitor_PhoneNumber" });
            DropTable("dbo.Visits");
            DropTable("dbo.Visitors");
            DropTable("dbo.PurposeLists");
            DropTable("dbo.Admins");
        }
    }
}
