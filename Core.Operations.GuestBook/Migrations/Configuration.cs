﻿namespace Core.Operations.GuestBook.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Core.Operations.GuestBook.Models;
    using Core.Operations.GuestBook.Services;

    internal sealed class Configuration : DbMigrationsConfiguration<Core.Operations.GuestBook.Models.GuestBookContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Core.Operations.GuestBook.Models.GuestBookContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            context.PurposeLists.AddOrUpdate(
                                new PurposeList { Purpose = "Interview" },
                                new PurposeList { Purpose = "Maintenance" },
                                new PurposeList { Purpose = "Personal" },
                                new PurposeList { Purpose = "Ex-Con" },
                                new PurposeList { Purpose = "Others" }
                );

            AdminService adminService = new AdminService();
            adminService.Register(new AdminDto()
            {
                Email = "nadia.ozone@sdworx.com",
                Password = "gbadmin1"
            });
            adminService.Register(new AdminDto()
            {
                Email = "rowin.gooriah@sdworx.com",
                Password = "gbadmin2"
            });

        }
    }
}
