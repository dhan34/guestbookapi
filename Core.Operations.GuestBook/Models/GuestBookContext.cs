using System.Data.Entity;

namespace Core.Operations.GuestBook.Models
{
    public class GuestBookContext : DbContext
    {
        public GuestBookContext() : base("GuestBookContext")
        { 
        }

        public static GuestBookContext Create()
        {
            return new GuestBookContext();
        }

        public DbSet<Admin> Admins { set; get; }
        public DbSet<Visitor> Visitors { set; get; }
        public DbSet<Visit> Visits { set; get; }

        public System.Data.Entity.DbSet<Core.Operations.GuestBook.Models.PurposeList> PurposeLists { get; set; }
    }
}