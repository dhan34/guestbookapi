﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Operations.GuestBook.Models
{
    public class Visit
    {
        [Key]
        public int VisitId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public string Purpose { get; set; }

        [Required]
        public DateTime TimeIn { get; set; }

        public DateTime? TimeOut { get; set; }

        public Visitor Visitor { get; set; }
    }
}
