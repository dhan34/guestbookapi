﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Core.Operations.GuestBook.Models
{
    public class Visitor
    {
        [Key]
        public String PhoneNumber { set; get; }

        [Required]
        public String FirstName { set; get; }

        [Required]
        public String LastName { set; get; }

        public String Email { set; get; }

        [JsonIgnore]
        public List<Visit> Visits { get; set; }

        public static implicit operator List<object>(Visitor v)
        {
            throw new NotImplementedException();
        }
    }
}

