using ClosedXML.Excel;
using Core.Operations.GuestBook.Data;
using Core.Operations.GuestBook.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Core.Operations.GuestBook.Services
{
    public class AdminService
    {
        readonly GuestBookContext _db;
       //X509EncryptingCredentials
        public AdminService()
        {
            _db = new GuestBookContext();
        }

        /// <summary>
        /// Used to login a user
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public Response<string> Login(AdminDto dto)
        {
            Response<string> response = new Response<string>();

            Admin _admin = _db.Admins.FirstOrDefault(a => a.Email == dto.Email);
            if(_admin == null) // wrong email
            {
                response.Success = false;
                response.Message = "User not found";
            } else if(!VerifyPasswordHash(dto.Password, _admin.PasswordHash, _admin.PasswordSalt)) // wrong password
            {
                response.Success = false;
                response.Message = "Wrong password.";
            } else
            {
                response.Data = CreateToken(dto);
            }

            return response;

        }

        /// <summary>
        /// Used to register a new admin.
        /// Mainly used for seeding
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Response<bool> Register(AdminDto user)
        {
            Response<bool> response = new Response<bool>();
            CreatePasswordHash(user.Password, out byte[] passwordHash, out byte[] passwordSalt);
            Admin admin = new Admin();
            admin.PasswordHash = passwordHash;
            admin.PasswordSalt = passwordSalt;
            admin.Email = user.Email;
            _db.Admins.Add(admin);
            _db.SaveChanges();
            response.Data = true;
            return response;
        }

        public async Task<Response<bool>> GenerateReportAsync(DateTime start, DateTime end)
        {
            //start = DateTime.ParseExact(start.ToString(), "dd-MM-yyyy", null);
            //end = DateTime.ParseExact(end.ToString(), "dd-MM-yyyy", null);
            Response<bool> response = new Response<bool>();
            List<Visit> visits =  _db.Visits.Where(v => v.Date >= start && v.Date <= end).Include(a => a.Visitor).ToList();

            var workbook = new XLWorkbook();
            workbook.AddWorksheet("sheetName");
            var ws = workbook.Worksheet("sheetName");

            ws.Cells("A1:I1").Style.Font.Bold = true;

            ws.Cell("A" + 1.ToString()).Value = "Visit ID";
            ws.Cell("B" + 1.ToString()).Value = "Date";
            ws.Cell("C" + 1.ToString()).Value = "First Name";
            ws.Cell("D" + 1.ToString()).Value = "Last Name";
            ws.Cell("E" + 1.ToString()).Value = "Phone number";
            ws.Cell("F" + 1.ToString()).Value = "Email Address";
            ws.Cell("G" + 1.ToString()).Value = "Time in";
            ws.Cell("H" + 1.ToString()).Value = "Time out";
            ws.Cell("I" + 1.ToString()).Value = "Purpose";

            int row = 2;

           
            foreach (Visit item in visits)
            {
                ws.Cell("A" + row.ToString()).Value = item.VisitId.ToString();
                ws.Cell("B" + row.ToString()).Value = item.Date;
                ws.Cell("C" + row.ToString()).Value = item.Visitor.FirstName;
                ws.Cell("D" + row.ToString()).Value = item.Visitor.LastName;
                ws.Cell("E" + row.ToString()).Value = item.Visitor.PhoneNumber;
                ws.Cell("F" + row.ToString()).Value = item.Visitor.Email;
                ws.Cell("G" + row.ToString()).Value = item.TimeIn.ToString().Split(' ')[1];
                if (item.TimeOut != null) {
                    ws.Cell("H" + row.ToString()).Value = item.TimeOut.ToString().Split(' ')[1];
                }
                else {
                    ws.Cell("H" + row.ToString()).Value = item.TimeOut;
                        }
                ws.Cell("I" + row.ToString()).Value = item.Purpose;

                row++;
            }

            if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Content"))){
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Content"));
            }
            
            var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Content/"), "Report.xlsx");
            workbook.SaveAs(path);

            return response;
        }


        private string CreateToken(AdminDto admin)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, admin.Email.ToString()),
                new Claim(ClaimTypes.Role, "Admin")
            };

            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("fdsakjfdskjfdslkjfsfdsfdsfds"));

            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds,
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
        
        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i])
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        //public void Email(string htmlString)
        //{
        //    try
        //    {
        //        MailMessage message = new MailMessage();
        //        SmtpClient smtp = new SmtpClient();
        //        message.From = new MailAddress("sdguestbook@outlook.com");
        //        message.To.Add(new MailAddress("sdguestbook@outlook.com"));
        //        message.Subject = "Test";
        //        message.IsBodyHtml = true; //to make message body as html  
        //        message.Body = htmlString;
        //        smtp.Port = 587;
        //        smtp.Host = "smtp.gmail.com"; //for gmail host  
        //        smtp.EnableSsl = true;
        //        smtp.UseDefaultCredentials = false;
        //        smtp.Credentials = new NetworkCredential("sdguestbook@outlook.com", "internship2020");
        //        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
        //        smtp.Send(message);
        //    }
        //    catch (Exception) { }
        //}

    }
}
