using Core.Operations.GuestBook.Data;
using Core.Operations.GuestBook.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Operations.GuestBook.Services
{
    public class GuestService
    {

        GuestBookContext db;
        public GuestService()
        {
            db = new GuestBookContext();
        }
        public async Task<Response<bool>> AddVisitAsync(CheckinDto dto)
        {
            Response<bool> response = new Response<bool>();

            // cannot check in if checked in today and time out is null
            Visit visit = db.Visits.Where(x => x.Date == DateTime.Today && x.TimeOut == null && x.Visitor.PhoneNumber == dto.PhoneNumber).FirstOrDefault();
            if (visit != null)
            {
                response.Success = false;
                response.Message = "Failed to check in without first checking-out";
                return response;
            }

            Visitor visitor = new Visitor()
            {
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                PhoneNumber = dto.PhoneNumber,
                Email = dto.Email,
            };

            // TimeZone localZone = TimeZone.CurrentTimeZone;
            // DateTime currentTime = DateTime.Now;

            TimeZoneInfo mrutimezone = TimeZoneInfo.FindSystemTimeZoneById("Mauritius Standard Time");
            DateTime currentTime = DateTime.UtcNow;
            currentTime = TimeZoneInfo.ConvertTimeFromUtc(currentTime, mrutimezone);

            visit = new Visit()
            {
                Purpose = dto.Purpose,
                TimeIn = currentTime,
                //TimeOut = DateTime.Now,
                Date = DateTime.Today,
                Visitor = visitor,
            };

            //Verify if Phone Number exists into the database
            Visitor v = db.Visitors.Where(i => i.PhoneNumber == visitor.PhoneNumber).FirstOrDefault();
            //if not, add a registration into visitor and visit
            if (v == null)
            {
                db.Visitors.Add(visitor);
                await db.SaveChangesAsync();

                db.Visits.Add(visit);
                await db.SaveChangesAsync();
            }
            //if yes, add entry into visit only
            else
            {
                visit.Visitor = v;
                db.Visits.Add(visit);
                await db.SaveChangesAsync();
            }

            return response;
        }

        public async Task<Response<bool>> Check_Out(String PhoneNumber)
        {
            Response<bool> response = new Response<bool>();

            Visitor v = await db.Visitors.FindAsync(PhoneNumber);
            if (v == null)
            {
                response.Success = false;
                response.Message = "Visitor not found";
                return response;
            }
            //Check if time out is null, to check user out
            Visit visit = db.Visits.Where(x => x.Date == DateTime.Today && x.TimeOut == null && x.Visitor.PhoneNumber == v.PhoneNumber).FirstOrDefault();
            if (visit == null)
            {
                response.Success = false;
                response.Message = "Visitor not checked in";
                return response;
            }

            TimeZoneInfo mrutimezone = TimeZoneInfo.FindSystemTimeZoneById("Mauritius Standard Time");
            DateTime currentTime = DateTime.UtcNow;
            currentTime = TimeZoneInfo.ConvertTimeFromUtc(currentTime, mrutimezone);

            visit.TimeOut = currentTime;

            await db.SaveChangesAsync();

            return response;
        }

        public Response<List<Visit>> VisitorsToday()
        {
            Response<List<Visit>> response = new Response<List<Visit>>();
            List<Visit> visits = db.Visits.Where(v => v.Date.Year == DateTime.Today.Year && v.Date.Month == DateTime.Today.Month && v.Date.Day == DateTime.Today.Day).Include(a => a.Visitor).ToList();

            if (visits.Count == 0)
            {
                response.Success = false;
                response.Message = "No entry found";
            }
            else
            {
                response.Data = visits;
            }

            return response;

        }

        // GET: api/PurposeLists
        public Response<List<PurposeList>> GetPurposeListAsync()
        {
            Response<List<PurposeList>> response = new Response<List<PurposeList>>();
            List<PurposeList> purpose = db.PurposeLists.ToList();
            response.Data = purpose;
            return response;

        }

    }
}




