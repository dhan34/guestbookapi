﻿using Core.Operations.GuestBook.Data;
using Core.Operations.GuestBook.EmailServer;
using Core.Operations.GuestBook.Models;
using Core.Operations.GuestBook.Services;
using System;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using TaxiPlannerAPI.Controllers;

namespace MU_SDGuestBookAPI.Controllers
{
    public class AdminController : ApiController
    {
        AdminService _adminService;
        GuestService _guestService;

        public AdminController()
        {
            _adminService = new AdminService();
            _guestService = new GuestService();
        }

        [HttpPost]
        [Route("api/Login")]
        public virtual HttpResponseMessage AdminLogin(AdminDto admin)
        {
            Response<string> response = new Response<string>();

            try
            {
                response = _adminService.Login(admin);
                if (response.Success)
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                else
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, response);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, $"Error : {e.Message}");
            }

        }

        [Authorizer]
        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("api/GetAll")]
        public virtual IHttpActionResult GetToday()
        {
            try
            {
                return Ok(_guestService.VisitorsToday());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [Authorizer]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("api/GenerateReport")]
        public virtual async Task<IHttpActionResult> GenerateReportAsync(DateTime start, DateTime end)
        {
            string date_from1 = "";
            string date_to1 = "";
            date_from1 += start.ToString("dddd dd MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US"));

            date_to1 += end.ToString("dddd dd MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US"));

            var admin_email = Thread.CurrentPrincipal.Identity.Name;
            try
            {
                Response<bool> res = await _adminService.GenerateReportAsync(start, end);
                EmailActions.SendEmail(admin_email, date_from1, date_to1);
                return Ok(res);
                

            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

    }
}
