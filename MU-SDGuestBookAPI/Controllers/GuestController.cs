﻿using Core.Operations.GuestBook.Data;
using Core.Operations.GuestBook.Models;
using Core.Operations.GuestBook.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace MU_SDGuestBookAPI.Controllers
{
    public class GuestController : ApiController
    {
        GuestService service;

        public GuestController()
        {
            service = new GuestService();
        }

        [HttpPost]
        [Route("api/CheckIn")]
        public virtual async Task<HttpResponseMessage> CheckIn(CheckinDto dto)
        {
            try
            {
                Response<bool> result = await service.AddVisitAsync(dto);

                if (result.Success)
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, result);

            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, $"Error : {e.Message}");
            }
        }

        [HttpGet]
        [Route("api/CheckOut")]
        public virtual async Task<HttpResponseMessage> CheckOut(string PhoneNumber)
        {
            try
            {
                Response<bool> result = await service.Check_Out(PhoneNumber);

                if (result.Success)
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, $"Error : {e.Message}");
            }
        }

        //[HttpGet]
        //[Route("api/GetGuest")]
        //public virtual HttpResponseMessage GetGuest(string phonenum)
        //{
        //    try
        //    {
        //        Response<Visitor> result = service.GetGuestAsync(phonenum);

        //        if (result.Success)
        //            return Request.CreateResponse(HttpStatusCode.OK, result);
        //        else
        //            return Request.CreateResponse(HttpStatusCode.NotFound, result);
        //    }
        //    catch (Exception e)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.NotFound, $"Error : {e.Message}");
        //    }
        //}

        [HttpGet]
        [Route("api/PurposeLists")]
        // GET: api/PurposeLists
        public virtual HttpResponseMessage GetPurposeLists()
        {
            try
            {
                Response<List<PurposeList>> result = service.GetPurposeListAsync();
                if (result.Success)

                    return Request.CreateResponse(HttpStatusCode.OK, result);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, result);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, $"Error : {e.Message}");
            }
        }
    }
}


